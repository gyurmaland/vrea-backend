<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Vrea</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

    <!-- use the font -->
    <style>
        body {
            font-family: 'Roboto', sans-serif;
            font-size: 48px;
        }
        a {
            text-decoration: none;
            color: white!important;
        }
        a[href] {
            color: white!important;
        }
        h2 {
            color: white;
        }
        p {
            color: white;
        }
    </style>
</head>
<body style="margin: 0; padding: 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; color: white">
    <tr>
        <td align="center" bgcolor="rgb(3, 46, 116)" style="padding: 40px 0 30px 0;">
            <h2>Kaptál egy új üzenetet</h2>
        </td>
    </tr>
    <tr>
        <td bgcolor="#311B92" style="padding: 40px 30px 40px 30px;">
            <p>Szia ${name}!</p>
            <p>Kaptál egy új üzenetet, melyet itt tudsz megnézni: </p>
            <p>${link}</p>
        </td>
    </tr>
</table>

</body>
</html>