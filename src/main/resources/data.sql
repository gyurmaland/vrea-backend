INSERT INTO skill (id, skill_enum) VALUES
  (1, 'GARDENING'),
  (2, 'BABYSITTING'),
  (3, 'DRIVING')
  ON CONFLICT (id) DO NOTHING;