package hu.vrea.converter;

import hu.vrea.exception.ResourceNotFoundException;
import hu.vrea.model.Review;
import hu.vrea.model.Work;
import hu.vrea.payload.review.ReviewRequest;
import hu.vrea.payload.review.ReviewResponse;
import hu.vrea.service.WorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReviewConverter {

    @Autowired
    WorkService workService;

    public Review convertReviewRequestToReview(ReviewRequest reviewRequest) {
        Review review = new Review();
        Work work = workService.findById(reviewRequest.getWorkId()).orElseThrow(() -> new ResourceNotFoundException("Work", "id", reviewRequest.getWorkId()));
        review.setRating(reviewRequest.getRating());
        review.setRatingText(reviewRequest.getRatingText());
        // TODO: itt majd dinamikusan kell tolni, hogy éppen ki lett review-zva az adott work-ön belül (worker vagy normal_user)
        review.setIsWorkerReviewed(true);
        review.setWork(work);
        return review;
    }

    public ReviewResponse convertReviewToReviewResponse(Review review) {
        ReviewResponse reviewResponse = new ReviewResponse();
        reviewResponse.setRating(review.getRating());
        reviewResponse.setRatingText(review.getRatingText());
        return reviewResponse;
    }
}
