package hu.vrea.converter;

import hu.vrea.action.GoogleMapAction;
import hu.vrea.model.DateTimeInterval;
import hu.vrea.model.Skill;
import hu.vrea.model.User;
import hu.vrea.model.Work;
import hu.vrea.model.enums.SkillEnum;
import hu.vrea.payload.interval.IntervalPayload;
import hu.vrea.payload.interval.IntervalStatus;
import hu.vrea.payload.location.LocationPayload;
import hu.vrea.payload.user.UserResponse;
import hu.vrea.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class UserConverter {

    @Autowired
    GoogleMapAction googleMapAction;

    @Autowired
    SkillService skillService;

    //TODO: kiszervezni
    String fullDatePattern = "yyy-MM-dd HH:mm:ss";

    String datePattern = "yyy-MM-dd";

    DateTimeFormatter fmtFullDate = DateTimeFormatter.ofPattern(fullDatePattern);

    DateTimeFormatter fmtDate = DateTimeFormatter.ofPattern(datePattern);

    public UserResponse convertUserToUserResponse(User user) {
        UserResponse userResponse = new UserResponse();
        userResponse.setId(user.getId());
        userResponse.setName(user.getName());
        userResponse.setEmail(user.getEmail());
        userResponse.setImageUrl(user.getImageUrl());
        userResponse.setProvider(user.getProvider().toString());
        userResponse.setProviderId(user.getProviderId());
        userResponse.setProfileDescription(user.getProfileDescription());
        if (Objects.nonNull(user.getBirthDate())) {
            userResponse.setBirthDate(user.getBirthDate().format(fmtDate));
        }
        userResponse.setJob(user.getJob());
        userResponse.setHighestEducation(user.getHighestEducation());
        userResponse.setProviderId(user.getProviderId());
        userResponse.setProviderId(user.getProviderId());
        if (Objects.nonNull(user.getSkills())) {
            List<SkillEnum> skillEnumList = new ArrayList<>();
            for (Skill skill : user.getSkills()) {
                skillEnumList.add(skill.getSkillEnum());
            }
            userResponse.setSkillList(skillEnumList);
        }

        LocationPayload locationPayload = new LocationPayload();
        if (Objects.nonNull(user.getLocation())) {
            locationPayload.setLat(user.getLocation().getCoordinates()[0].y);
            locationPayload.setLng(user.getLocation().getCoordinates()[0].x);
            userResponse.setLocationPayload(locationPayload);
        }

        if (!StringUtils.isEmpty(user.getGooglePlaceId())) {
            userResponse.setGooglePlaceId(user.getGooglePlaceId());
        }

        if (!StringUtils.isEmpty(user.getGooglePlaceName())) {
            userResponse.setGooglePlaceName(user.getGooglePlaceName());
        }

        List<IntervalPayload> intervalPayloadList = new ArrayList<>();
        for (DateTimeInterval interval : user.getDateTimeIntervalList()) {
            IntervalPayload intervalPayload = new IntervalPayload();
            intervalPayload.setId(interval.getId());
            intervalPayload.setStart(interval.getStartDateTime().format(fmtFullDate));
            intervalPayload.setEnd(interval.getEndDateTime().format(fmtFullDate));

            boolean isTimeIntervalPending = false;
            for (Work work : interval.getWorkList()) {
                if (Objects.isNull(work.getAccepted())) {
                    isTimeIntervalPending = true;
                    break;
                }
            }

            if (Objects.nonNull(interval.getWorkList()) && interval.getStartDateTime().isAfter(LocalDateTime.now()) && interval.getWorkList().isEmpty()) {
                intervalPayload.setIntervalStatus(IntervalStatus.FREE);
            } else if (Objects.nonNull(interval.getWorkList()) && interval.getStartDateTime().isAfter(LocalDateTime.now()) && isTimeIntervalPending) {
                intervalPayload.setIntervalStatus(IntervalStatus.PENDING);
            } else {
                intervalPayload.setIntervalStatus(IntervalStatus.NOT_AVAILABLE);
            }

            intervalPayloadList.add(intervalPayload);
        }
        userResponse.setIntervalPayloadList(intervalPayloadList);

        return userResponse;
    }
}
