package hu.vrea.payload.interval;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IntervalPayload {

    private Long id;

    private String start;

    private String end;

    private IntervalStatus intervalStatus;
}
