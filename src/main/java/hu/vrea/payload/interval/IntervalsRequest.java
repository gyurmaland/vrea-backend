package hu.vrea.payload.interval;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class IntervalsRequest {

    private List<IntervalPayload> intervalPayloadList;
}
