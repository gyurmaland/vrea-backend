package hu.vrea.payload.interval;

public enum IntervalStatus {
    FREE,
    NOT_AVAILABLE,
    PENDING
}
