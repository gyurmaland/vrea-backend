package hu.vrea.payload.googlemaps;

import hu.vrea.payload.googlemaps.GooglePlaceResultResponse;

public class GoogleMapsResponse {

    private GooglePlaceResultResponse result;

    private String status;

    public GooglePlaceResultResponse getResult() {
        return result;
    }

    public void setResult(GooglePlaceResultResponse result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
