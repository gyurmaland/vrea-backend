package hu.vrea.payload.googlemaps;

import hu.vrea.payload.location.LocationPayload;

public class GoogleGeometryResponse {

    private LocationPayload location;

    public LocationPayload getLocation() {
        return location;
    }

    public void setLocation(LocationPayload location) {
        this.location = location;
    }
}
