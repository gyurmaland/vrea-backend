package hu.vrea.payload.googlemaps;

public class GooglePlaceResultResponse {

    private String name;

    private GoogleGeometryResponse geometry;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GoogleGeometryResponse getGeometry() {
        return geometry;
    }

    public void setGeometry(GoogleGeometryResponse geometry) {
        this.geometry = geometry;
    }
}
