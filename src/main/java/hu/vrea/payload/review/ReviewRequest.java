package hu.vrea.payload.review;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class ReviewRequest {

    @NotNull
    private int rating;

    @NotNull
    @NotEmpty
    @Size(min = 5, max = 1024)
    private String ratingText;

    @NotNull
    private Long workId;
}
