package hu.vrea.payload.review;

import hu.vrea.payload.PagingRequest;
import lombok.Getter;
import lombok.Setter;

public class ReviewListRequest {

    @Getter
    @Setter
    private PagingRequest pagingRequest;

    @Getter
    @Setter
    private ReviewSearchRequest reviewSearchRequest;
}
