package hu.vrea.payload.review;

import lombok.Getter;
import lombok.Setter;

public class ReviewSearchRequest {

    @Getter
    @Setter
    private Long userId;
}
