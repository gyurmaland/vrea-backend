package hu.vrea.payload.review;

import lombok.Getter;
import lombok.Setter;

public class ReviewResponse {

    @Getter
    @Setter
    private int rating;

    @Getter
    @Setter
    private String ratingText;
}
