package hu.vrea.payload.notification;

import hu.vrea.model.enums.NotificationType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationPayload {

    private NotificationType notificationType;

    private Object notificationPayloadObject;

    private Long connectedWorkId;
}
