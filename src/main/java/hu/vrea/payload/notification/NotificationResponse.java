package hu.vrea.payload.notification;

import hu.vrea.model.enums.NotificationType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationResponse {

    private Long notificationId;

    private NotificationType notificationType;

    private Long connectedWorkId;

    private String createdDate;
}
