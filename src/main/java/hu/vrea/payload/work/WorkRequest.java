package hu.vrea.payload.work;

import hu.vrea.model.enums.SkillEnum;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class WorkRequest {

    @NotNull
    @NotEmpty
    @Size(min = 2, max = 30)
    private String title;

    @NotNull
    private SkillEnum requiredSkill;

    @NotNull
    private Long dateTimeIntervalId;

    @NotNull
    @NotEmpty
    @Size(min = 5, max = 255)
    private String startMsg;

    @NotNull
    private Long workerId;
}
