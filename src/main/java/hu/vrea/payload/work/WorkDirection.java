package hu.vrea.payload.work;

public enum WorkDirection {

    INCOMING,
    OUTGOING
}
