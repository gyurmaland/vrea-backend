package hu.vrea.payload.work;

public enum WorkStatus {
    // munka elfogadásra vár
    PENDING,

    // munka elfgoadva, de még aktív (nincs elvégezve se elkezdve)
    WORK_ACCEPTED,

    // munka elfgoadva, de még aktív  (nincs elvégezdve de már el van kezdben)
    WORK_IN_PROGRESS,

    // munka el lett fogadva és már lezárult sikeresen (el van végezve és értékelhető)
    WORK_DONE,

    // munka el lett fogadva, lezárult sikeresen és értékelve is lett
    WORK_DONE_AND_REVIEWED,

    // munka elutasítva
    WORK_DENIED,

    // munkára nem érkezett reagálás (se nem fogadták el, se nem utasították vissza) és lejárt az ideje
    WORK_CLOSED
}
