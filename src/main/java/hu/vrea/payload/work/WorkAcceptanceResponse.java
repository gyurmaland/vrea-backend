package hu.vrea.payload.work;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkAcceptanceResponse {

    private Long id;

    private Boolean accepted;
}
