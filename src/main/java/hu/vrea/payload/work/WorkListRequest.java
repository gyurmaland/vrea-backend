package hu.vrea.payload.work;

import hu.vrea.payload.PagingRequest;

public class WorkListRequest {

    private PagingRequest pagingRequest;

    private WorkSearchRequest workSearchRequest;

    public PagingRequest getPagingRequest() {
        return pagingRequest;
    }

    public void setPagingRequest(PagingRequest pagingRequest) {
        this.pagingRequest = pagingRequest;
    }

    public WorkSearchRequest getWorkSearchRequest() {
        return workSearchRequest;
    }

    public void setWorkSearchRequest(WorkSearchRequest workSearchRequest) {
        this.workSearchRequest = workSearchRequest;
    }
}
