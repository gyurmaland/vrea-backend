package hu.vrea.payload.work;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkSearchRequest {

    private WorkDirection workDirection;

    private WorkStatus workStatus;
}
