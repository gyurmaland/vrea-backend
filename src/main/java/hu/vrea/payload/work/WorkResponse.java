package hu.vrea.payload.work;

import hu.vrea.model.enums.SkillEnum;
import hu.vrea.payload.interval.IntervalPayload;
import hu.vrea.payload.review.ReviewResponse;
import hu.vrea.payload.user.UserResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class WorkResponse {

    private Long id;

    private String title;

    private SkillEnum requiredSkill;

    private IntervalPayload intervalPayload;

    private String startMsg;

    private Boolean accepted;

    private UserResponse worker;

    private UserResponse normalUser;

    private List<ReviewResponse> reviewResponseList;
}
