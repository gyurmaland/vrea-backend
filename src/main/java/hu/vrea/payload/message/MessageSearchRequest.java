package hu.vrea.payload.message;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageSearchRequest {
    
    private Long workId;
}
