package hu.vrea.payload.message;

import lombok.Getter;
import lombok.Setter;

public class MessageResponse {

    @Getter
    @Setter
    private Long workId;

    @Getter
    @Setter
    private String dateTime;

    @Getter
    @Setter
    private String text;

    @Getter
    @Setter
    private String senderName;
}
