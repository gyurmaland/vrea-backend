package hu.vrea.payload.message;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class MessageRequest {

    @NotNull
    private Long workId;

    @NotNull
    @NotEmpty
    private String senderName;

    @NotNull
    @NotEmpty
    @Size(min = 1)
    private String text;
}
