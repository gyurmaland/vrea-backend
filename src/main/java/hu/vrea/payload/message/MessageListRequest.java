package hu.vrea.payload.message;

import hu.vrea.payload.PagingRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageListRequest {

    private PagingRequest pagingRequest;

    private MessageSearchRequest messageSearchRequest;
}
