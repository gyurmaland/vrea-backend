package hu.vrea.payload.location;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class LocationPayload {

    @NotEmpty
    private double lat;

    @NotEmpty
    private double lng;
}
