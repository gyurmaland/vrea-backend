package hu.vrea.payload.user;

import hu.vrea.payload.PagingRequest;

public class UserListRequest {

    private PagingRequest pagingRequest;

    private UserSearchRequest userSearchRequest;

    public PagingRequest getPagingRequest() {
        return pagingRequest;
    }

    public void setPagingRequest(PagingRequest pagingRequest) {
        this.pagingRequest = pagingRequest;
    }

    public UserSearchRequest getUserSearchRequest() {
        return userSearchRequest;
    }

    public void setUserSearchRequest(UserSearchRequest userSearchRequest) {
        this.userSearchRequest = userSearchRequest;
    }
}
