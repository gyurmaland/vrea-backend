package hu.vrea.payload.user;

import hu.vrea.model.enums.SkillEnum;
import hu.vrea.model.UserListOrderBy;
import hu.vrea.payload.location.LocationPayload;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserSearchRequest {

    private SkillEnum skill;

    private LocationPayload location;

    private String placeName;

    private String workDateTime;

    private UserListOrderBy userListOrderBy;
}
