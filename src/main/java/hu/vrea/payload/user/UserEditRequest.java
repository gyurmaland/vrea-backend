package hu.vrea.payload.user;

import hu.vrea.model.enums.HighestEducation;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class UserEditRequest {

    @NotNull
    @NotEmpty
    private List<String> skillList;

    @NotNull
    @NotBlank
    private String googlePlaceId;

    @NotNull
    @NotBlank
    private String googlePlaceName;

    private String profileDescription;

    private String birthDate;

    private String job;

    private HighestEducation highestEducation;
}
