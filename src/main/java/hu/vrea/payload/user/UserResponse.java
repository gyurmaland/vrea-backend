package hu.vrea.payload.user;

import hu.vrea.model.enums.HighestEducation;
import hu.vrea.model.enums.SkillEnum;
import hu.vrea.payload.interval.IntervalPayload;
import hu.vrea.payload.location.LocationPayload;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserResponse {

    private Long id;

    private String name;

    private String email;

    private String imageUrl;

    private String provider;

    private String providerId;

    private String profileDescription;

    private String birthDate;

    private String job;

    private HighestEducation highestEducation;

    private List<SkillEnum> skillList;

    private LocationPayload locationPayload;

    private String googlePlaceId;

    private String googlePlaceName;

    private List<IntervalPayload> intervalPayloadList;
}
