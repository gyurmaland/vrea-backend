package hu.vrea.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DateTimeRequest {

    private String dateTime;
}
