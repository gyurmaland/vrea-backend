package hu.vrea.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class Review extends Auditable<String> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Getter
    @Setter
    private int rating;

    @Getter
    @Setter
    private String ratingText;

    @Getter
    @Setter
    private Boolean isWorkerReviewed;

    @ManyToOne
    @JoinColumn(name = "work_id")
    @Getter
    @Setter
    private Work work;

}
