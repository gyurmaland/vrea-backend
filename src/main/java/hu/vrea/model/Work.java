package hu.vrea.model;

import hu.vrea.model.enums.SkillEnum;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.util.List;

@Entity
public class Work extends Auditable<String> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private SkillEnum requiredSkill;

    @ManyToOne
    @JoinColumn(name = "date_time_interval_id")
    @Getter
    @Setter
    private DateTimeInterval dateTimeInterval;

    @Getter
    @Setter
    private String startMsg;

    @Nullable
    @Getter
    @Setter
    private Boolean accepted;

    @ManyToOne
    @JoinColumn(name = "worker_id")
    @Getter
    @Setter
    private User worker;

    @ManyToOne
    @JoinColumn(name = "normal_user_id")
    @Getter
    @Setter
    private User normalUser;

    @OneToMany(mappedBy = "work", fetch = FetchType.LAZY)
    @Getter
    @Setter
    private List<Review> reviews;

    @OneToMany(mappedBy = "work", fetch = FetchType.LAZY)
    @Getter
    @Setter
    private List<Message> messages;
}
