package hu.vrea.model;

public enum AuthProvider {
    local,
    facebook,
    google,
    github
}
