package hu.vrea.model.enums;

public enum SkillEnum {
    GARDENING,
    BABYSITTING,
    DRIVING,
    CUSTOM
}
