package hu.vrea.model.enums;

public enum HighestEducation {

    ELEMENTARY,
    MIDDLE,
}
