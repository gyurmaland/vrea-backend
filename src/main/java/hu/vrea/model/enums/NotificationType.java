package hu.vrea.model.enums;

public enum NotificationType {
    NEW_WORK_REQUEST,
    WORK_REQUEST_ACCEPTED,
    WORK_REQUEST_DENIED,
    NEW_REVIEW,
    NEW_MESSAGE,
}
