package hu.vrea.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class DateTimeInterval {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime startDateTime;

    private LocalDateTime endDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "worker_id")
    private User worker;

    @OneToMany(mappedBy = "dateTimeInterval", fetch = FetchType.LAZY)
    private List<Work> workList;

    public DateTimeInterval() {
    }

    public DateTimeInterval(LocalDateTime startDateTime, LocalDateTime endDateTime, User worker) {
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.worker = worker;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(LocalDateTime endDateTime) {
        this.endDateTime = endDateTime;
    }

    public User getWorker() {
        return worker;
    }

    public void setWorker(User worker) {
        this.worker = worker;
    }

    public List<Work> getWorkList() {
        return workList;
    }

    public void setWorkList(List<Work> workList) {
        this.workList = workList;
    }
}
