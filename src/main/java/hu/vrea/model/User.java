package hu.vrea.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import hu.vrea.model.enums.HighestEducation;
import hu.vrea.serializer.JsonToPointDeserializer;
import hu.vrea.serializer.PointToJsonSerializer;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Formula;
import org.locationtech.jts.geom.Point;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "appuser", uniqueConstraints = {
        @UniqueConstraint(columnNames = "email")
})
@Getter
@Setter
public class User extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String name;

    private String username;

    private String profileDescription;

    private LocalDate birthDate;

    private String job;

    @Enumerated(EnumType.STRING)
    private HighestEducation highestEducation;

    @Email
    @Column(nullable = false)
    private String email;

    private String imageUrl;

    @Column(nullable = false)
    private Boolean emailVerified = false;

    @JsonIgnore
    private String password;

    @NotNull
    @Enumerated(EnumType.STRING)
    private AuthProvider provider;

    private String providerId;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "User_Skill",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "skill_id")}
    )
    private List<Skill> skills;

    @OneToMany(mappedBy = "worker", fetch = FetchType.LAZY)
    private List<DateTimeInterval> dateTimeIntervalList;

    @JsonSerialize(using = PointToJsonSerializer.class)
    @JsonDeserialize(using = JsonToPointDeserializer.class)
    @Column(columnDefinition = "geography(POINT, 4326)")
    private Point location;

    private String googlePlaceId;

    private String googlePlaceName;

    @Formula("(select count(dti.worker_id) from appuser au " +
            "left join work w on au.id = w.worker_id " +
            "inner join date_time_interval dti on au.id = dti.worker_id " +
            "where au.id = id and dti.start_date_time > current_timestamp and w.accepted is null)")
    private Integer openIntervalCount;

    @OneToMany(mappedBy = "worker", fetch = FetchType.LAZY)
    private List<Work> workerWorks;

    @OneToMany(mappedBy = "normalUser", fetch = FetchType.LAZY)
    private List<Work> normalUserWorks;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Notification> notificationList;
}
