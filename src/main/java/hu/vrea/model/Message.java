package hu.vrea.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class Message extends Auditable<String> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Getter
    @Setter
    private String text;

    @ManyToOne
    @JoinColumn(name = "work_id")
    @Getter
    @Setter
    private Work work;

    @Getter
    @Setter
    private String senderName;

}
