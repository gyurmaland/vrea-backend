package hu.vrea.controller;

import hu.vrea.action.ReviewAction;
import hu.vrea.payload.review.ReviewListRequest;
import hu.vrea.payload.review.ReviewRequest;
import hu.vrea.payload.review.ReviewResponse;
import hu.vrea.security.CurrentUser;
import hu.vrea.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ReviewController {

    @Autowired
    ReviewAction reviewAction;

    @PostMapping("/review/save")
    @PreAuthorize("hasRole('USER')")
    public ReviewResponse saveReview(@CurrentUser UserPrincipal userPrincipal, @RequestBody @Valid ReviewRequest reviewRequest) {
        return reviewAction.saveReview(userPrincipal, reviewRequest);
    }

    @PostMapping("/reviews")
    @PreAuthorize("hasRole('USER')")
    public Page<ReviewResponse> listReviews(@RequestBody @Valid ReviewListRequest reviewListRequest) {
        return reviewAction.findReviews(reviewListRequest);
    }

    @GetMapping("/review/worker-rating-avg/{userId}")
    @PreAuthorize("hasRole('USER')")
    public Double reviewAvg(@PathVariable Long userId) {
        return reviewAction.avgWorkerRatingByUserId(userId);
    }
}
