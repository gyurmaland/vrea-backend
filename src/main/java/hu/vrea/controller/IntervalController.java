package hu.vrea.controller;

import hu.vrea.action.IntervalAction;
import hu.vrea.payload.DateTimeRequest;
import hu.vrea.payload.interval.IntervalPayload;
import hu.vrea.payload.interval.IntervalsRequest;
import hu.vrea.security.CurrentUser;
import hu.vrea.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class IntervalController {

    @Autowired
    private IntervalAction intervalAction;

    @GetMapping("/intervals")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity findIntervalsByLoggedInUser(@CurrentUser UserPrincipal userPrincipal) {
        List<IntervalPayload> dateTimeIntervalList = intervalAction.findIntervalsByWorkerId(userPrincipal.getId());
        return new ResponseEntity(dateTimeIntervalList, HttpStatus.OK);
    }

    @PostMapping("/intervals/selected-date")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity findByStartDateTimeBetween(@CurrentUser UserPrincipal userPrincipal, @RequestBody @Valid DateTimeRequest dateTimeRequest) {
        List<IntervalPayload> dateTimeIntervalList = intervalAction.findByStartDateTimeBetween(userPrincipal.getId(), dateTimeRequest.getDateTime());
        return new ResponseEntity(dateTimeIntervalList, HttpStatus.OK);
    }

    @PutMapping("/interval-edit")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity editInterval(@CurrentUser UserPrincipal userPrincipal, @RequestBody @Valid IntervalsRequest intervalsRequest) {
        intervalAction.saveIntervals(userPrincipal, intervalsRequest);
        return new ResponseEntity(HttpStatus.OK);
    }
}
