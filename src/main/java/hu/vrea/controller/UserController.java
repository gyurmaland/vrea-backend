package hu.vrea.controller;

import hu.vrea.action.UserAction;
import hu.vrea.payload.user.UserEditRequest;
import hu.vrea.payload.user.UserListRequest;
import hu.vrea.payload.user.UserResponse;
import hu.vrea.security.CurrentUser;
import hu.vrea.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class UserController {

    @Autowired
    private UserAction userAction;

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public UserResponse getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
        return userAction.findById(userPrincipal.getId());
    }

    @GetMapping("/user/{userId}")
    @PreAuthorize("hasRole('USER')")
    public UserResponse getUserById(@PathVariable Long userId) {
        return userAction.findById(userId);
    }

    @PostMapping("/workers")
    @PreAuthorize("hasRole('USER')")
    public Page<UserResponse> findWorkers(@CurrentUser UserPrincipal userPrincipal, @RequestBody @Valid UserListRequest userListRequest) {
        return userAction.findWorkers(userPrincipal, userListRequest);
    }

    @PutMapping("/user/edit")
    @PreAuthorize("hasRole('USER')")
    public UserResponse updateCurrentUser(@CurrentUser UserPrincipal userPrincipal, @Valid @RequestBody UserEditRequest userEditRequest) {
        return userAction.updateUser(userPrincipal.getId(), userEditRequest);
    }
}
