package hu.vrea.controller;

import hu.vrea.action.NotificationAction;
import hu.vrea.payload.notification.NotificationResponse;
import hu.vrea.security.CurrentUser;
import hu.vrea.security.UserPrincipal;
import hu.vrea.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class NotificationController {

    @Autowired
    NotificationService notificationService;

    @Autowired
    NotificationAction notificationAction;

    @GetMapping("/notification-count")
    @PreAuthorize("hasRole('USER')")
    public Long getCurrentUserNotificationCount(@CurrentUser UserPrincipal userPrincipal) {
        return notificationService.countAllByUserIdAndSeenIsFalse(userPrincipal.getId());
    }

    @GetMapping("/notifications/list")
    @PreAuthorize("hasRole('USER')")
    public List<NotificationResponse> getCurrentUserNotificationsByConnectedId(@CurrentUser UserPrincipal userPrincipal) {
        return notificationAction.findAllByUserIdAndSeenIsFalse(userPrincipal.getId());
    }

    @PutMapping("/notification-seen/{notificationId}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity setUserNotificationToSeenById(@CurrentUser UserPrincipal userPrincipal, @PathVariable Long notificationId) {
        notificationAction.setUserNotificationToSeenById(userPrincipal.getId(), notificationId);
        return new ResponseEntity(HttpStatus.OK);
    }
}
