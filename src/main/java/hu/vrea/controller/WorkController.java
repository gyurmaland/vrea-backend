package hu.vrea.controller;

import hu.vrea.action.WorkAction;
import hu.vrea.payload.work.WorkAcceptancePayload;
import hu.vrea.payload.work.WorkListRequest;
import hu.vrea.payload.work.WorkRequest;
import hu.vrea.payload.work.WorkResponse;
import hu.vrea.security.CurrentUser;
import hu.vrea.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class WorkController {

    @Autowired
    private WorkAction workAction;

    @GetMapping("/work/{workId}")
    @PreAuthorize("hasRole('USER')")
    public WorkResponse findWorkById(@CurrentUser UserPrincipal userPrincipal, @PathVariable String workId) {
        return workAction.findWorkById(userPrincipal, new Long(workId));
    }

    @PostMapping("/work/save-work")
    @PreAuthorize("hasRole('USER')")
    public WorkResponse saveWork(@CurrentUser UserPrincipal userPrincipal, @Valid @RequestBody WorkRequest workRequest) {
        return workAction.saveWork(workRequest, userPrincipal);
    }

    @PostMapping("/work/list")
    @PreAuthorize("hasRole('USER')")
    public Page<WorkResponse> workList(@CurrentUser UserPrincipal userPrincipal, @Valid @RequestBody WorkListRequest workListRequest) {
        return workAction.findAllAdvancedSearch(userPrincipal, workListRequest);
    }

    @GetMapping("/work/work-done-count/{userId}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity getWorkDoneCountByWorkerId(@PathVariable String userId) {
        return new ResponseEntity(workAction.getWorkDoneCountByWorkerId(new Long(userId)), HttpStatus.OK);
    }

    @PostMapping("/work/my-work-list")
    @PreAuthorize("hasRole('USER')")
    public Page<WorkResponse> getMyWorkList(@CurrentUser UserPrincipal userPrincipal, @Valid @RequestBody WorkListRequest workListRequest) {
        return workAction.findAllByWorkerId(userPrincipal, workListRequest);
    }

    @PostMapping("/work/my-work-request-list")
    @PreAuthorize("hasRole('USER')")
    public Page<WorkResponse> getMyWorkRequestList(@CurrentUser UserPrincipal userPrincipal, @Valid @RequestBody WorkListRequest workListRequest) {
        return workAction.findAllByWorkerId(userPrincipal, workListRequest);
    }

    @PostMapping("/work/set-work-acceptance")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity setWorkAcceptance(@CurrentUser UserPrincipal userPrincipal, @Valid @RequestBody WorkAcceptancePayload workAcceptancePayload) {
        workAction.setWorkAcceptance(userPrincipal, workAcceptancePayload);
        return new ResponseEntity(workAcceptancePayload.getAccepted(), HttpStatus.OK);
    }
}
