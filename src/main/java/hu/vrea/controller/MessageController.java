package hu.vrea.controller;

import hu.vrea.action.MessageAction;
import hu.vrea.payload.message.MessageListRequest;
import hu.vrea.payload.message.MessageRequest;
import hu.vrea.payload.message.MessageResponse;
import hu.vrea.security.CurrentUser;
import hu.vrea.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class MessageController {

    @Autowired
    MessageAction messageAction;

    @PostMapping("/message/save")
    @PreAuthorize("hasRole('USER')")
    public MessageResponse saveMessage(@CurrentUser UserPrincipal userPrincipal, @RequestBody @Valid MessageRequest messageRequest) {
        return messageAction.saveMessageAndSendToUser(userPrincipal, messageRequest);
    }

    @PostMapping("/messages")
    @PreAuthorize("hasRole('USER')")
    public Page<MessageResponse> listMessages(@CurrentUser UserPrincipal userPrincipal, @RequestBody @Valid MessageListRequest messageListRequest) {
        return messageAction.findAllByWorkId(userPrincipal, messageListRequest);
    }
}
