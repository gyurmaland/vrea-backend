package hu.vrea.email;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import hu.vrea.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private Configuration freemarkerConfig;

    public void constructAndSendEmail(MailType mailType, User user, String subject, String link) {
        Mail mail = new Mail();
        mail.setFrom("gyuri@vrea.hu");
        mail.setTo(user.getEmail());
        mail.setSubject(subject);

        Map<String, String> model = new HashMap<>();
        model.put("name", user.getName());
        model.put("link", link);
        mail.setModel(model);

        try {
            sendSimpleMessage(mail, mailType);
        } catch (MessagingException | IOException | TemplateException e) {
            e.printStackTrace();
        }
    }

    private void sendSimpleMessage(Mail mail, MailType mailType) throws IOException, TemplateException, javax.mail.MessagingException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());

        freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/templates");

        Template t = freemarkerConfig.getTemplate("greeting-email-template.ftl");

        if (mailType == MailType.NEW_WORK_REQUEST) {
            t = freemarkerConfig.getTemplate("new-work-email-template.ftl");
        }
        if (mailType == MailType.WORK_REQUEST_ACCEPTED) {
            t = freemarkerConfig.getTemplate("work-accepted-email-template.ftl");
        }
        if (mailType == MailType.NEW_MESSAGE) {
            t = freemarkerConfig.getTemplate("new-message-email-template.ftl");
        }
        if (mailType == MailType.NEW_REVIEW) {
            t = freemarkerConfig.getTemplate("new-review-email-template.ftl");
        }

        String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, mail.getModel());

        helper.setTo(mail.getTo());
        helper.setText(html, true);
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());

        emailSender.send(message);
    }
}
