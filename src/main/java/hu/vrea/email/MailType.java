package hu.vrea.email;

public enum MailType {
    GREETING,
    NEW_WORK_REQUEST,
    WORK_REQUEST_ACCEPTED,
    NEW_MESSAGE,
    NEW_REVIEW
}
