package hu.vrea.repository;

import hu.vrea.model.DateTimeInterval;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface DateTimeIntervalRepository extends JpaRepository<DateTimeInterval, Long>, QuerydslPredicateExecutor<DateTimeInterval> {

    List<DateTimeInterval> findByWorkerId(Long userId);

    List<DateTimeInterval> findByStartDateTimeBetweenAndWorkerId(LocalDateTime startDateTime, LocalDateTime endDateTime, Long workerId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE DateTimeInterval dti SET dti.startDateTime = :startDateTime, dti.endDateTime = :endDateTime WHERE dti.id = :id")
    void editById(Long id, LocalDateTime startDateTime, LocalDateTime endDateTime);

    void deleteByWorkerId(Long workerId);
}
