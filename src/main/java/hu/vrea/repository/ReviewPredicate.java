package hu.vrea.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import hu.vrea.model.QReview;

import java.util.Objects;

public class ReviewPredicate {

    public static Predicate reviewSearch(Long reviewedUserId) {
        BooleanBuilder where = new BooleanBuilder();

        // TODO: ha később kell, csekkolni, hogy a reviewedUserId munkás-e vagy sem
        if (Objects.nonNull(reviewedUserId)) {
            where.and(QReview.review.work.worker.id.eq(reviewedUserId));
        }

        return where;
    }
}
