package hu.vrea.repository;

import hu.vrea.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long>, QuerydslPredicateExecutor<Review> {

    @Query("SELECT AVG(r.rating) FROM Review r WHERE r.work.worker.id = :userId")
    Double avgWorkerRatingByUserId(@Param("userId") Long userId);

}
