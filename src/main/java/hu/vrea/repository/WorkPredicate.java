package hu.vrea.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import hu.vrea.model.QWork;
import hu.vrea.payload.work.WorkDirection;
import hu.vrea.payload.work.WorkSearchRequest;
import hu.vrea.payload.work.WorkStatus;

import java.time.LocalDateTime;
import java.util.Objects;

public class WorkPredicate {

    private WorkPredicate() {
    }

    public static Predicate workSearch(Long currentUserId, WorkSearchRequest workSearchRequest) {
        BooleanBuilder where = new BooleanBuilder();

        if (Objects.nonNull(currentUserId) && workSearchRequest.getWorkDirection() == WorkDirection.INCOMING) {
            where.or(QWork.work.worker.id.eq(currentUserId));
        }

        if (Objects.nonNull(currentUserId) && workSearchRequest.getWorkDirection() == WorkDirection.OUTGOING) {
            where.or(QWork.work.normalUser.id.eq(currentUserId));
        }

        if (workSearchRequest.getWorkStatus() == WorkStatus.PENDING) {
            where.and(QWork.work.accepted.isNull())
                    .and(QWork.work.dateTimeInterval.startDateTime.before(LocalDateTime.now()));
        }

        if (workSearchRequest.getWorkStatus() == WorkStatus.WORK_ACCEPTED) {
            where.and(QWork.work.accepted.eq(true))
                    .and(QWork.work.dateTimeInterval.startDateTime.before(LocalDateTime.now()));
        }

        if (workSearchRequest.getWorkStatus() == WorkStatus.WORK_IN_PROGRESS) {
            where.and(QWork.work.accepted.eq(true))
                    .and(QWork.work.dateTimeInterval.startDateTime.before(LocalDateTime.now())
                    .and(QWork.work.dateTimeInterval.endDateTime.after(LocalDateTime.now())));
        }

        if (workSearchRequest.getWorkStatus() == WorkStatus.WORK_DONE) {
            where.and(QWork.work.accepted.eq(true))
                    .and(QWork.work.dateTimeInterval.endDateTime.after(LocalDateTime.now()))
                    .and(QWork.work.reviews.isEmpty());
        }

        if (workSearchRequest.getWorkStatus() == WorkStatus.WORK_DONE_AND_REVIEWED) {
            where.and(QWork.work.accepted.eq(true))
                    .and(QWork.work.dateTimeInterval.endDateTime.after(LocalDateTime.now()))
                    .and(QWork.work.reviews.isNotEmpty());
        }

        if (workSearchRequest.getWorkStatus() == WorkStatus.WORK_DENIED) {
            where.and(QWork.work.accepted.eq(false))
                    .and(QWork.work.dateTimeInterval.endDateTime.after(LocalDateTime.now()));
        }

        if (workSearchRequest.getWorkStatus() == WorkStatus.WORK_CLOSED) {
            where.and(QWork.work.accepted.isNull())
                    .and(QWork.work.dateTimeInterval.endDateTime.before(LocalDateTime.now()));
        }

        return where;
    }
}
