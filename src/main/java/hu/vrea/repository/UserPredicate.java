package hu.vrea.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import hu.vrea.model.QUser;
import hu.vrea.payload.user.UserSearchRequest;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class UserPredicate {

    final static String datePattern = "yyy-MM-dd";
    final static DateTimeFormatter fmtDate = DateTimeFormatter.ofPattern(datePattern);

    private UserPredicate() {
    }

    public static Predicate workerSearch(Long currentUserId, UserSearchRequest userSearchRequest) {
        BooleanBuilder where = new BooleanBuilder();

        if (!StringUtils.isEmpty(currentUserId)) {
            where.and(QUser.user.id.ne(currentUserId));
        }

        if (Objects.nonNull(userSearchRequest.getSkill())) {
            where.and(QUser.user.skills.any().skillEnum.eq(userSearchRequest.getSkill()));
        }

        if (!StringUtils.isEmpty(userSearchRequest.getWorkDateTime())) {
            LocalDateTime searchStartOfDay = LocalDate.parse(userSearchRequest.getWorkDateTime(), fmtDate).atStartOfDay();
            LocalDateTime searchEndOfDay = LocalDate.parse(userSearchRequest.getWorkDateTime(), fmtDate).atTime(LocalTime.MAX);
            where.and(QUser.user.dateTimeIntervalList.any().startDateTime.between(searchStartOfDay, searchEndOfDay));
        }

        return where;
    }
}
