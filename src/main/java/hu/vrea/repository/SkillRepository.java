package hu.vrea.repository;

import hu.vrea.model.Skill;
import hu.vrea.model.enums.SkillEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillRepository extends JpaRepository<Skill, Long>, QuerydslPredicateExecutor<Skill> {

    Skill findBySkillEnum(SkillEnum skillEnum);
}
