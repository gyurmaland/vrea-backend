package hu.vrea.repository;

import hu.vrea.model.User;
import org.locationtech.jts.geom.Point;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, QuerydslPredicateExecutor<User> {

    @Query(value = "SELECT u FROM User u JOIN u.skills s " +
            "WHERE (u.id <> :currentUserId) " +
            "AND (:skillName = '' OR (lower(s.skillEnum) = lower(:skillName))) " +
            "AND (cast(:searchStartOfDay as timestamp) is null OR (u.id = (SELECT dti.worker.id FROM DateTimeInterval dti WHERE dti.worker.id <> :currentUserId AND dti.startDateTime BETWEEN :searchStartOfDay AND :searchEndOfDay))) " +
            "ORDER BY ST_DISTANCE(u.location, :point) ASC, u.openIntervalCount DESC")
    Page<User> findWorkersOrderedByDistance(@Param("point") Point point,
                                            @Param("currentUserId") Long currentUserId,
                                            @Param("skillName") String skillName,
                                            @Param("searchStartOfDay") LocalDateTime searchStartOfDay,
                                            @Param("searchEndOfDay") LocalDateTime searchEndOfDay,
                                            Pageable pageable);

    @Query(value = "SELECT u FROM User u " +
            "WHERE (u.id <> :currentUserId) " +
            "AND (cast(:searchStartOfDay as timestamp) is null OR (u.id = (SELECT dti.worker.id FROM DateTimeInterval dti WHERE dti.worker.id <> :currentUserId AND dti.startDateTime BETWEEN :searchStartOfDay AND :searchEndOfDay))) " +
            "ORDER BY ST_DISTANCE(u.location, :point) ASC, u.openIntervalCount DESC")
    Page<User> findWorkersOrderedByDistanceWithoutSkillName(@Param("point") Point point,
                                                            @Param("currentUserId") Long currentUserId,
                                                            @Param("searchStartOfDay") LocalDateTime searchStartOfDay,
                                                            @Param("searchEndOfDay") LocalDateTime searchEndOfDay,
                                                            Pageable pageable);

    Optional<User> findByEmail(String email);

    Boolean existsByEmail(String email);

}
