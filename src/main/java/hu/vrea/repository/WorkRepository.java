package hu.vrea.repository;

import hu.vrea.model.Work;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

public interface WorkRepository extends JpaRepository<Work, Long>, QuerydslPredicateExecutor<Work> {

    Page<Work> findAllByWorkerIdOrderByCreatedDateDesc(Long workerId, Pageable pageable);

    Page<Work> findAllByNormalUserIdOrderByCreatedDateDesc(Long normalUserId, Pageable pageable);

    int countByAcceptedAndWorker_IdAndDateTimeInterval_EndDateTimeBefore(boolean accepted, Long workerId, LocalDateTime dateTime);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Work w set w.accepted = :accepted where w.worker.id = :workerId and w.id = :workId")
    void setWorkAcceptance(Boolean accepted, Long workerId, Long workId);
}
