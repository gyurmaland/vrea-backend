package hu.vrea.repository;

import hu.vrea.model.Notification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long>, QuerydslPredicateExecutor<Notification> {

    int countAllByUserIdAndSeenIsFalse(Long userId);

    List<Notification> findAllByUserIdAndSeenIsFalse(Long userId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Notification n set n.seen = true where n.user.id = :userId and n.id = :notificationId")
    void setUserNotificationToSeenById(Long userId, Long notificationId);
}
