package hu.vrea;

import hu.vrea.config.AppProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class VreaApplication {

	public static void main(String[] args) {
		SpringApplication.run(VreaApplication.class, args);
	}
}
