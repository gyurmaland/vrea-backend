package hu.vrea.action;

import hu.vrea.exception.BadRequestException;
import hu.vrea.model.DateTimeInterval;
import hu.vrea.model.User;
import hu.vrea.model.Work;
import hu.vrea.payload.interval.IntervalPayload;
import hu.vrea.payload.interval.IntervalStatus;
import hu.vrea.payload.interval.IntervalsRequest;
import hu.vrea.security.UserPrincipal;
import hu.vrea.service.DateTimeIntervalService;
import hu.vrea.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
public class IntervalAction {

    @Autowired
    private DateTimeIntervalService dateTimeIntervalService;

    @Autowired
    private UserService userService;

    //TODO: kiszervezni
    final private String datePattern = "yyy-MM-dd";

    final private String fullDatePattern = "yyy-MM-dd HH:mm:ss";

    final private DateTimeFormatter fmtFullDate = DateTimeFormatter.ofPattern(fullDatePattern);
    final private DateTimeFormatter fmtDate = DateTimeFormatter.ofPattern(datePattern);

    public List<IntervalPayload> findIntervalsByWorkerId(Long workerId) {
        List<DateTimeInterval> dateTimeIntervalList = dateTimeIntervalService.findByWorkerId(workerId);
        List<IntervalPayload> intervalPayloadList = new ArrayList<>();
        for (DateTimeInterval dateTimeInterval : dateTimeIntervalList) {
            intervalPayloadList.add(convertIntervalToIntervalPayload(dateTimeInterval));
        }
        return intervalPayloadList;
    }

    public List<IntervalPayload> findByStartDateTimeBetween(Long userId, String dateTimeRequest) {
        LocalDateTime startDateTime = LocalDate.parse(dateTimeRequest, fmtDate).atStartOfDay();
        LocalDateTime endDateTime = LocalDate.parse(dateTimeRequest, fmtDate).atTime(LocalTime.MAX);
        List<DateTimeInterval> dateTimeIntervalList = dateTimeIntervalService.findByStartDateTimeBetween(userId, startDateTime, endDateTime);
        List<IntervalPayload> intervalPayloadList = new ArrayList<>();
        for (DateTimeInterval dateTimeInterval : dateTimeIntervalList) {
            intervalPayloadList.add(convertIntervalToIntervalPayload(dateTimeInterval));
        }
        return intervalPayloadList;
    }

    public void saveIntervals(UserPrincipal userPrincipal, IntervalsRequest intervalsRequest) {
        User user = userService.findUserById(userPrincipal.getId()).orElseThrow(() ->
                new UsernameNotFoundException("User not found with ID : " + userPrincipal.getId()));
        checkValidityOfIntervals(userPrincipal, intervalsRequest.getIntervalPayloadList());
        for (IntervalPayload intervalPayload : intervalsRequest.getIntervalPayloadList()) {
            Optional<DateTimeInterval> dateTimeIntervalOptional = dateTimeIntervalService.findById(intervalPayload.getId());
            DateTimeInterval dateTimeInterval = new DateTimeInterval(
                    LocalDateTime.parse(intervalPayload.getStart(), fmtFullDate),
                    LocalDateTime.parse(intervalPayload.getEnd(), fmtFullDate),
                    user);
            if (dateTimeIntervalOptional.isPresent()) {
                dateTimeIntervalService.editById(dateTimeIntervalOptional.get().getId(), dateTimeInterval);
            } else {
                dateTimeIntervalService.save(dateTimeInterval);
            }
        }
    }

    private void checkValidityOfIntervals(UserPrincipal userPrincipal, List<IntervalPayload> intervalPayloadList) {
        List<DateTimeInterval> dateTimeIntervalList = dateTimeIntervalService.findByWorkerId(userPrincipal.getId());
        for (IntervalPayload intervalPayload : intervalPayloadList) {
            LocalDateTime startTime = LocalDateTime.parse(intervalPayload.getStart(), fmtFullDate);
            LocalDateTime endTIme = LocalDateTime.parse(intervalPayload.getEnd(), fmtFullDate);
            //TODO: megcsinálni új típussal
/*            Interval interval = new Interval(startTime, endTIme);

            if (startTime.isAfter(endTIme)) {
                throw new BadRequestException("Start time can't be after end time!");
            }

            if (interval.toDurationMillis() < 1800000) {
                throw new BadRequestException("There must be 30 minutes between the two date times!");
            }*/

            // TODO: ez valamiért szar, utána kell nézni
/*            if (startTime.isBefore(new DateTime())) {
                throw new BadRequestException("The start date time must be in the future!");
            }*/

            for (DateTimeInterval dateTimeInterval : dateTimeIntervalList) {
                if ((startTime.isAfter(dateTimeInterval.getStartDateTime()) && startTime.isBefore(dateTimeInterval.getEndDateTime()) ||
                        (endTIme.isAfter(dateTimeInterval.getStartDateTime()) && endTIme.isBefore(dateTimeInterval.getEndDateTime())))) {
                    throw new BadRequestException("Intervals cant match with each other.");
                }
            }
        }
    }

    private IntervalPayload convertIntervalToIntervalPayload(DateTimeInterval dateTimeInterval) {
        IntervalPayload intervalPayload = new IntervalPayload();
        intervalPayload.setId(dateTimeInterval.getId());
        intervalPayload.setStart(dateTimeInterval.getStartDateTime().format((fmtFullDate)));
        intervalPayload.setEnd(dateTimeInterval.getEndDateTime().format((fmtFullDate)));

        //TODO: duplikátum (kiszervez)
        boolean isTimeIntervalPending = false;
        for (Work work : dateTimeInterval.getWorkList()) {
            if (Objects.isNull(work.getAccepted())) {
                isTimeIntervalPending = true;
                break;
            }
        }

        if (Objects.nonNull(dateTimeInterval.getWorkList()) && dateTimeInterval.getStartDateTime().isAfter(LocalDateTime.now()) && dateTimeInterval.getWorkList().isEmpty()) {
            intervalPayload.setIntervalStatus(IntervalStatus.FREE);
        } else if (Objects.nonNull(dateTimeInterval.getWorkList()) && dateTimeInterval.getStartDateTime().isAfter(LocalDateTime.now()) && isTimeIntervalPending) {
            intervalPayload.setIntervalStatus(IntervalStatus.PENDING);
        } else {
            intervalPayload.setIntervalStatus(IntervalStatus.NOT_AVAILABLE);
        }

        return intervalPayload;
    }

}
