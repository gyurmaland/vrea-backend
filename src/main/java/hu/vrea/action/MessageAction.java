package hu.vrea.action;

import hu.vrea.exception.ResourceNotFoundException;
import hu.vrea.exception.UnauthorizedException;
import hu.vrea.model.Message;
import hu.vrea.model.User;
import hu.vrea.model.Work;
import hu.vrea.model.enums.NotificationType;
import hu.vrea.payload.message.MessageListRequest;
import hu.vrea.payload.message.MessageRequest;
import hu.vrea.payload.message.MessageResponse;
import hu.vrea.security.UserPrincipal;
import hu.vrea.service.MessageService;
import hu.vrea.service.NotificationService;
import hu.vrea.service.UserService;
import hu.vrea.service.WorkService;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MessageAction {

    @Autowired
    private MessageService messageService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private WorkService workService;

    @Autowired
    private UserService userService;

    //TODO: kiszervezni
    String fullDatePattern = "yyy-MM-dd HH:mm:ss";

    public MessageResponse saveMessageAndSendToUser(UserPrincipal userPrincipal, MessageRequest messageRequest) {
        //TODO: túl sok van itt lekérdezve, ez lehet így lassabb lesz, refakt lehet szükséges a jövőben
        Message message = messageService.save(convertMessageRequestToMessage(messageRequest));
        MessageResponse messageResponse = convertMessageToMessageResponse(message);
        User senderUser = userService.findUserById(userPrincipal.getId()).orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId()));
        if (message.getWork().getWorker().getId().equals(senderUser.getId())) {
            notificationService.sendNotificationToUser(NotificationType.NEW_MESSAGE, message.getWork().getNormalUser(), message.getWork().getId(), messageResponse);
        } else {
            notificationService.sendNotificationToUser(NotificationType.NEW_MESSAGE, message.getWork().getWorker(), message.getWork().getId(), messageResponse);
        }
        return messageResponse;
    }

    public Page<MessageResponse> findAllByWorkId(UserPrincipal userPrincipal, MessageListRequest messageListRequest) {
        PageRequest pageRequest = PageRequest.of(messageListRequest.getPagingRequest().getPage(),
                messageListRequest.getPagingRequest().getPageSize());
        Page<Message> messagePage = messageService.findAllByWorkId(messageListRequest.getMessageSearchRequest().getWorkId(), pageRequest);

        List<MessageResponse> messageResponseList = new ArrayList<>();

        for (Message message : messagePage.getContent()) {
            if (!(userPrincipal.getId().equals(message.getWork().getWorker().getId()) || userPrincipal.getId().equals(message.getWork().getNormalUser().getId()))) {
                throw new UnauthorizedException("Nana :)");
            }
            messageResponseList.add(convertMessageToMessageResponse(message));
        }

        return new PageImpl<>(messageResponseList, PageRequest
                .of(messageListRequest.getPagingRequest().getPage(),
                        messageListRequest.getPagingRequest().getPageSize()), messagePage.getTotalElements());
    }

    private Message convertMessageRequestToMessage(MessageRequest messageRequest) {
        Message message = new Message();
        Work work = workService.findById(messageRequest.getWorkId()).orElseThrow(() -> new ResourceNotFoundException("Work", "id", messageRequest.getWorkId()));
        message.setWork(work);
        message.setSenderName(messageRequest.getSenderName());
        message.setText(messageRequest.getText());
        return message;
    }

    private MessageResponse convertMessageToMessageResponse(Message message) {
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setWorkId(message.getWork().getId());
        messageResponse.setSenderName(message.getSenderName());
        messageResponse.setText(message.getText());
        DateTimeFormatter fmt = DateTimeFormat.forPattern(fullDatePattern);
        messageResponse.setDateTime(new DateTime(message.getCreatedDate()).toString(fmt));
        return messageResponse;
    }


}
