package hu.vrea.action;

import hu.vrea.converter.UserConverter;
import hu.vrea.exception.ResourceNotFoundException;
import hu.vrea.model.QUser;
import hu.vrea.model.Skill;
import hu.vrea.model.User;
import hu.vrea.model.UserListOrderBy;
import hu.vrea.model.enums.SkillEnum;
import hu.vrea.payload.location.LocationPayload;
import hu.vrea.payload.user.UserEditRequest;
import hu.vrea.payload.user.UserListRequest;
import hu.vrea.payload.user.UserResponse;
import hu.vrea.payload.user.UserSearchRequest;
import hu.vrea.security.UserPrincipal;
import hu.vrea.service.DateTimeIntervalService;
import hu.vrea.service.SkillService;
import hu.vrea.service.UserService;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.querydsl.QSort;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@Component
public class UserAction {

    @Autowired
    private UserService userService;

    @Autowired
    private DateTimeIntervalService dateTimeIntervalService;

    @Autowired
    private SkillService skillService;

    @Autowired
    private GoogleMapAction googleMapAction;

    @Autowired
    private UserConverter userConverter;

    //TODO: kiszervezni
    String datePattern = "yyy-MM-dd";

    public UserResponse findById(Long userId) {
        User user = userService.findUserById(userId).orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));
        return userConverter.convertUserToUserResponse(user);
    }

    public UserResponse updateUser(Long userId, UserEditRequest userEditRequest) {
        User user = userService.findUserById(userId).orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));

        List<Skill> skillList = new ArrayList<>();
        //TODO: később majd itt lekezelni, ha egyéb más String jön be (CUSTOM)
        for (String skill : userEditRequest.getSkillList()) {
            if (Stream.of(SkillEnum.values()).anyMatch(s -> s.name().equals(skill))) {
                Skill skillInstance = skillService.findBySkillEnum(SkillEnum.valueOf(skill));
                skillList.add(skillInstance);
            }
        }
        user.setSkills(skillList);

        if (!StringUtils.isEmpty(userEditRequest.getGooglePlaceId())) {
            GeometryFactory gf = new GeometryFactory();
            LocationPayload locationPayload = googleMapAction.getGoogleGeometryLocationByPlaceId(userEditRequest.getGooglePlaceId());
            Point location = gf.createPoint(new Coordinate(locationPayload.getLng(), locationPayload.getLat()));
            location.setSRID(4326);
            user.setLocation(location);
        }

        user.setGooglePlaceId(userEditRequest.getGooglePlaceId());
        user.setGooglePlaceName(userEditRequest.getGooglePlaceName());
        user.setProfileDescription(userEditRequest.getProfileDescription());
        if (Objects.nonNull(userEditRequest.getBirthDate())) {
            user.setBirthDate(LocalDate.parse(userEditRequest.getBirthDate(), DateTimeFormatter.ofPattern(datePattern)));
        }
        user.setJob(userEditRequest.getJob());
        user.setHighestEducation(userEditRequest.getHighestEducation());
        user.setGooglePlaceName(userEditRequest.getGooglePlaceName());
        return userConverter.convertUserToUserResponse(userService.saveUser(user));
    }

    public Page<UserResponse> findWorkers(UserPrincipal userPrincipal, UserListRequest userListRequest) {
        QSort qSort = new QSort(QUser.user.openIntervalCount.desc());
        PageRequest pageRequest = PageRequest.of(userListRequest.getPagingRequest().getPage(),
                userListRequest.getPagingRequest().getPageSize(), qSort);

        Page<User> userPage = null;

        Long currentUserId = userPrincipal.getId();
        UserSearchRequest userSearchRequest = userListRequest.getUserSearchRequest();

        //TODO: refakt kell majd az tuti
        if (Objects.nonNull(userSearchRequest)) {
            UserListOrderBy userListOrderBy = userSearchRequest.getUserListOrderBy();
            if (Objects.nonNull(userListOrderBy)) {
                if (userListOrderBy == UserListOrderBy.DISTANCE_FROM_CURRENT_LOCATION) {
                    userPage = userService.findWorkersOrderedByDistance(currentUserId, userListRequest.getUserSearchRequest(), pageRequest);
                } else {
                    userPage = userService.findWorkers(currentUserId, userListRequest.getUserSearchRequest(), pageRequest);
                }
            }
        }

        List<UserResponse> userResponseList = new ArrayList<>();

        for (User user : userPage.getContent()) {
            userResponseList.add(userConverter.convertUserToUserResponse(user));
        }

        return new PageImpl<>(userResponseList, PageRequest
                .of(userListRequest.getPagingRequest().getPage(),
                        userListRequest.getPagingRequest().getPageSize()), userPage.getTotalElements());
    }
}
