package hu.vrea.action;

import hu.vrea.payload.googlemaps.GoogleMapsResponse;
import hu.vrea.payload.location.LocationPayload;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;

@Component
public class GoogleMapAction {

    @Value("${googleMapsProperties.googleMapKey}")
    private String googleMapkey;

    @Value("${googleMapsProperties.googleMapRestUrl}")
    private String googleMapRestUrl;

    public String getGooglePlaceNameByPlaceId(String placeId) {
        UriComponents builder = UriComponentsBuilder.fromHttpUrl(googleMapRestUrl)
                .queryParam("place_id", placeId)
                .queryParam("fields", "name")
                .queryParam("key", googleMapkey).build();

        RestTemplate restTemplate = new RestTemplate();

        GoogleMapsResponse googleMapsResponse = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, getEntity(), GoogleMapsResponse.class).getBody();

        return googleMapsResponse.getResult().getName();
    }

    public LocationPayload getGoogleGeometryLocationByPlaceId(String placeId) {
        UriComponents builder = UriComponentsBuilder.fromHttpUrl(googleMapRestUrl)
                .queryParam("place_id", placeId)
                .queryParam("fields", "name,geometry")
                .queryParam("key", googleMapkey).build();

        RestTemplate restTemplate = new RestTemplate();

        GoogleMapsResponse googleMapsResponse = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, getEntity(), GoogleMapsResponse.class).getBody();

        return googleMapsResponse.getResult().getGeometry().getLocation();
    }

    private HttpEntity<String> getEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
        return entity;
    }
}
