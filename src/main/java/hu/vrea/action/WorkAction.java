package hu.vrea.action;

import hu.vrea.converter.ReviewConverter;
import hu.vrea.converter.UserConverter;
import hu.vrea.exception.BadRequestException;
import hu.vrea.exception.ResourceNotFoundException;
import hu.vrea.model.QWork;
import hu.vrea.model.Review;
import hu.vrea.model.User;
import hu.vrea.model.Work;
import hu.vrea.model.enums.NotificationType;
import hu.vrea.payload.interval.IntervalPayload;
import hu.vrea.payload.review.ReviewResponse;
import hu.vrea.payload.work.*;
import hu.vrea.security.UserPrincipal;
import hu.vrea.service.DateTimeIntervalService;
import hu.vrea.service.NotificationService;
import hu.vrea.service.UserService;
import hu.vrea.service.WorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.data.querydsl.QSort;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class WorkAction {

    @Autowired
    private WorkService workService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserConverter userConverter;

    @Autowired
    private ReviewConverter reviewConverter;

    @Autowired
    private DateTimeIntervalService dateTimeIntervalService;

    @Autowired
    private NotificationService notificationService;

    public WorkResponse findWorkById(UserPrincipal currentUser, Long workId) {
        Work work = workService.findById(workId).orElseThrow(() -> new ResourceNotFoundException("Work", "id", workId));
        if (!(work.getNormalUser().getId().equals(currentUser.getId()) || work.getWorker().getId().equals(currentUser.getId()))) {
            throw new BadRequestException("Nana! :)");
        }
        return convertWorkToWorkResponse(workService.findById(workId).orElseThrow(() -> new ResourceNotFoundException("Work", "id", workId)));
    }

    public WorkResponse saveWork(WorkRequest workRequest, UserPrincipal currentUser) {
        WorkResponse workResponse = convertWorkToWorkResponse(workService.saveWork(convertWorkRequestToWork(workRequest, currentUser)));
        User user = userService.findUserById(workRequest.getWorkerId()).orElseThrow(() ->
                new UsernameNotFoundException("User not found with ID : " + workRequest.getWorkerId()));
        notificationService.sendNotificationToUser(NotificationType.NEW_WORK_REQUEST, user, workResponse.getId(), workResponse);
        return workResponse;
    }

    public Page<WorkResponse> findAllAdvancedSearch(UserPrincipal userPrincipal, WorkListRequest workListRequest) {
        QSort qSort = new QSort(QWork.work.createdDate.desc());

        QPageRequest qPageRequest = QPageRequest.of(workListRequest.getPagingRequest().getPage(),
                workListRequest.getPagingRequest().getPageSize(), qSort);

        Page<Work> workPage = workService.findAllAdvancedSearch(userPrincipal.getId(), workListRequest.getWorkSearchRequest(), qPageRequest);

        List<WorkResponse> workResponseList = new ArrayList<>();

        for (Work work : workPage.getContent()) {
            workResponseList.add(convertWorkToWorkResponse(work));
        }

        return new PageImpl<>(workResponseList, PageRequest
                .of(workListRequest.getPagingRequest().getPage(),
                        workListRequest.getPagingRequest().getPageSize()), workPage.getTotalElements());
    }

    public int getWorkDoneCountByWorkerId(Long workerId) {
        return workService.getWorkDoneCountByWorkerId(workerId);
    }

    public Page<WorkResponse> findAllByWorkerId(UserPrincipal userPrincipal, WorkListRequest workListRequest) {
        PageRequest pageRequest = PageRequest.of(workListRequest.getPagingRequest().getPage(),
                workListRequest.getPagingRequest().getPageSize());

        Page<Work> workPage = null;

        User currentUser = userService.findUserById(userPrincipal.getId()).orElseThrow(() ->
                new UsernameNotFoundException("User not found with ID : " + userPrincipal.getId()));
        WorkSearchRequest workSearchRequest = workListRequest.getWorkSearchRequest();

        if (Objects.nonNull(workSearchRequest)) {
            if (workSearchRequest.getWorkDirection() == WorkDirection.INCOMING) {
                workPage = workService.findAllByWorkerId(currentUser.getId(), pageRequest);
            } else {
                workPage = workService.findAllByNormalUserId(currentUser.getId(), pageRequest);
            }
        }

        List<WorkResponse> workResponseList = new ArrayList<>();

        for (Work work : workPage.getContent()) {
            workResponseList.add(convertWorkToWorkResponse(work));
        }

        return new PageImpl<>(workResponseList, PageRequest
                .of(workListRequest.getPagingRequest().getPage(),
                        workListRequest.getPagingRequest().getPageSize()), workPage.getTotalElements());
    }

    public void setWorkAcceptance(UserPrincipal userPrincipal, WorkAcceptancePayload workAcceptancePayload) {
        Work work = workService.findById(workAcceptancePayload.getWorkId()).orElseThrow(() ->
                new UsernameNotFoundException("Work not found with ID : " + workAcceptancePayload.getWorkId()));
        workService.setWorkAcceptance(workAcceptancePayload.getAccepted(), userPrincipal.getId(), workAcceptancePayload.getWorkId());
        // TODO: ez nem olyan szép megoldás
        work.setAccepted(workAcceptancePayload.getAccepted());
        if (workAcceptancePayload.getAccepted()) {
            notificationService.sendNotificationToUser(NotificationType.WORK_REQUEST_ACCEPTED, work.getNormalUser(), work.getId(), convertWorkToWorkAcceptanceResponse(work));
        } else {
            notificationService.sendNotificationToUser(NotificationType.WORK_REQUEST_DENIED, work.getNormalUser(), work.getId(), convertWorkToWorkAcceptanceResponse(work));
        }
    }

    private Work convertWorkRequestToWork(WorkRequest workRequest, UserPrincipal userPrincipal) {
        User currentUser = userService.findUserById(userPrincipal.getId()).orElseThrow(() ->
                new UsernameNotFoundException("User not found with ID : " + userPrincipal.getId()));
        Work work = new Work();
        work.setTitle(workRequest.getTitle());
        work.setRequiredSkill(workRequest.getRequiredSkill());
        work.setDateTimeInterval(dateTimeIntervalService.findById(workRequest.getDateTimeIntervalId()).orElseThrow(() ->
                new ResourceNotFoundException("DateTimeInterval", "id", workRequest.getDateTimeIntervalId())));
        work.setStartMsg(workRequest.getStartMsg());
        work.setAccepted(null);
        work.setNormalUser(userService.findUserById(currentUser.getId()).orElseThrow(() ->
                new UsernameNotFoundException("User not found with ID : " + currentUser.getId())));
        work.setWorker(userService.findUserById(workRequest.getWorkerId()).orElseThrow(() ->
                new UsernameNotFoundException("User not found with ID : " + workRequest.getWorkerId())));

        return work;
    }

    private WorkResponse convertWorkToWorkResponse(Work work) {
        WorkResponse workResponse = new WorkResponse();
        IntervalPayload intervalPayload = new IntervalPayload();
        //TODO: kiszervezni
        String fullDatePattern = "yyy-MM-dd HH:mm:ss";
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern(fullDatePattern);
        intervalPayload.setId(work.getDateTimeInterval().getId());
        intervalPayload.setStart(work.getDateTimeInterval().getStartDateTime().format(fmt));
        intervalPayload.setEnd(work.getDateTimeInterval().getEndDateTime().format(fmt));

        workResponse.setId(work.getId());
        workResponse.setTitle(work.getTitle());
        workResponse.setRequiredSkill(work.getRequiredSkill());
        workResponse.setIntervalPayload(intervalPayload);
        workResponse.setStartMsg(work.getStartMsg());
        workResponse.setAccepted(work.getAccepted());
        workResponse.setNormalUser(userConverter.convertUserToUserResponse(work.getNormalUser()));
        workResponse.setWorker(userConverter.convertUserToUserResponse(work.getWorker()));

        if (Objects.nonNull(work.getReviews())) {
            List<ReviewResponse> reviewResponseList = new ArrayList<>();
            for (Review review : work.getReviews()) {
                reviewResponseList.add(reviewConverter.convertReviewToReviewResponse(review));
            }
            workResponse.setReviewResponseList(reviewResponseList);
        }

        return workResponse;
    }

    private WorkAcceptanceResponse convertWorkToWorkAcceptanceResponse(Work work) {
        WorkAcceptanceResponse workAcceptanceResponse = new WorkAcceptanceResponse();

        workAcceptanceResponse.setId(work.getId());
        workAcceptanceResponse.setAccepted(work.getAccepted());

        return workAcceptanceResponse;
    }
}
