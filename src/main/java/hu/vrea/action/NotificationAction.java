package hu.vrea.action;

import hu.vrea.model.Notification;
import hu.vrea.payload.notification.NotificationResponse;
import hu.vrea.service.NotificationService;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class NotificationAction {

    @Autowired
    private NotificationService notificationService;

    //TODO: kiszervezni
    String fullDatePattern = "yyy-MM-dd HH:mm:ss";

    public List<NotificationResponse> findAllByUserIdAndSeenIsFalse(Long userId) {
        List<Notification> notificationList = notificationService.findAllByUserIdAndSeenIsFalse(userId);

        List<NotificationResponse> notificationResponseList = new ArrayList<>();

        for (Notification notification : notificationList) {
            notificationResponseList.add(convertNotificationToNotificationResponse(notification));
        }

        return notificationResponseList;
    }

    public void setUserNotificationToSeenById(Long userId, Long notificationId) {
        notificationService.setUserNotificationToSeenById(userId, notificationId);
    }

    private NotificationResponse convertNotificationToNotificationResponse(Notification notification) {
        NotificationResponse notificationResponse = new NotificationResponse();
        notificationResponse.setNotificationId(notification.getId());
        notificationResponse.setNotificationType(notification.getNotificationType());
        notificationResponse.setConnectedWorkId(notification.getWorkId());
        DateTimeFormatter fmt = DateTimeFormat.forPattern(fullDatePattern);
        notificationResponse.setCreatedDate(new DateTime(notification.getCreatedDate()).toString(fmt));
        return notificationResponse;
    }
}
