package hu.vrea.action;

import hu.vrea.converter.ReviewConverter;
import hu.vrea.model.QReview;
import hu.vrea.model.Review;
import hu.vrea.model.enums.NotificationType;
import hu.vrea.payload.review.ReviewListRequest;
import hu.vrea.payload.review.ReviewRequest;
import hu.vrea.payload.review.ReviewResponse;
import hu.vrea.security.UserPrincipal;
import hu.vrea.service.NotificationService;
import hu.vrea.service.ReviewService;
import hu.vrea.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.data.querydsl.QSort;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Component
public class ReviewAction {

    @Autowired
    ReviewService reviewService;

    @Autowired
    UserService userService;

    @Autowired
    NotificationService notificationService;

    @Autowired
    ReviewConverter reviewConverter;

    public Page<ReviewResponse> findReviews(ReviewListRequest reviewListRequest) {
        QSort qSort = new QSort(QReview.review.createdDate.desc());
        if (!StringUtils.isEmpty(reviewListRequest.getPagingRequest().getSortField())) {
            if (reviewListRequest.getPagingRequest().getSortField().equalsIgnoreCase("rating")) {
                if (reviewListRequest.getPagingRequest().getSortDirection().isAscending()) {
                    qSort = new QSort(QReview.review.rating.asc());
                } else {
                    qSort = new QSort(QReview.review.rating.desc());
                }
            }
        }
        QPageRequest qPageRequest = QPageRequest.of(reviewListRequest.getPagingRequest().getPage(),
                reviewListRequest.getPagingRequest().getPageSize(), qSort);

        Page<Review> reviewPage = reviewService.findReviews(reviewListRequest.getReviewSearchRequest().getUserId(), qPageRequest);

        List<ReviewResponse> reviewResponseList = new ArrayList<>();

        for (Review review : reviewPage.getContent()) {
            reviewResponseList.add(reviewConverter.convertReviewToReviewResponse(review));
        }

        return new PageImpl<>(reviewResponseList, PageRequest
                .of(reviewListRequest.getPagingRequest().getPage(),
                        reviewListRequest.getPagingRequest().getPageSize()), reviewPage.getTotalElements());
    }

    public ReviewResponse saveReview(UserPrincipal userPrincipal, ReviewRequest reviewRequest) {
        Review review = reviewConverter.convertReviewRequestToReview(reviewRequest);
        // TODO: itt validálni kell, hogy valóban az a user adja-e a review-t aki a munkát kérte
        // TODO: valamint itt kell majd módosítás, ha akarunk visszacsatoló review-t is
        notificationService.sendNotificationToUser(NotificationType.NEW_REVIEW, review.getWork().getWorker(), review.getWork().getId(), reviewRequest);
        return reviewConverter.convertReviewToReviewResponse(reviewService.saveReviewForWorker(review));
    }

    public Double avgWorkerRatingByUserId(Long userId) {
        return reviewService.avgWorkerRatingByUserId(userId);
    }
}
