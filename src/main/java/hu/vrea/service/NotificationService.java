package hu.vrea.service;

import hu.vrea.email.EmailService;
import hu.vrea.email.MailType;
import hu.vrea.model.Notification;
import hu.vrea.model.User;
import hu.vrea.model.enums.NotificationType;
import hu.vrea.payload.notification.NotificationPayload;
import hu.vrea.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationService {

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private EmailService emailService;

    @Autowired
    private NotificationRepository notificationRepository;

    public void sendNotificationToUser(NotificationType notificationType, User user, Long connectedWorkId, Object payload) {
        Notification notification = new Notification();
        notification.setNotificationType(notificationType);
        notification.setUser(user);
        notification.setWorkId(connectedWorkId);
        notification.setSeen(false);

        NotificationPayload notificationPayload = new NotificationPayload();
        notificationPayload.setNotificationType(notificationType);
        notificationPayload.setConnectedWorkId(connectedWorkId);
        notificationPayload.setNotificationPayloadObject(payload);

        notificationRepository.save(notification);

        if (notificationType == NotificationType.NEW_WORK_REQUEST) {
            emailService.constructAndSendEmail(MailType.NEW_WORK_REQUEST, user, "Új megbízást kaptál", "vrea.netlify.app");
        }
        if (notificationType == NotificationType.WORK_REQUEST_ACCEPTED) {
            emailService.constructAndSendEmail(MailType.WORK_REQUEST_ACCEPTED, user, "Megbízásdat elfogadták", "vrea.netlify.app");
        }
        if (notificationType == NotificationType.NEW_MESSAGE) {
            emailService.constructAndSendEmail(MailType.NEW_MESSAGE, user, "Új üzenetet kaptál", "vrea.netlify.app");
        }
        if (notificationType == NotificationType.NEW_REVIEW) {
            emailService.constructAndSendEmail(MailType.NEW_REVIEW, user, "Új értékelést kaptál", "vrea.netlify.app");
        }

        template.convertAndSendToUser(
                user.getEmail(),
                "/queue/notify",
                notificationPayload
        );
    }

    public long countAllByUserIdAndSeenIsFalse(Long userId) {
        return notificationRepository.countAllByUserIdAndSeenIsFalse(userId);
    }

    public List<Notification> findAllByUserIdAndSeenIsFalse(Long userId) {
        return notificationRepository.findAllByUserIdAndSeenIsFalse(userId);
    }

    public void setUserNotificationToSeenById(Long userId, Long notificationId) {
        notificationRepository.setUserNotificationToSeenById(userId, notificationId);
    }


}
