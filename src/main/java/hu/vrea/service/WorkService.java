package hu.vrea.service;

import hu.vrea.model.Work;
import hu.vrea.payload.work.WorkSearchRequest;
import hu.vrea.repository.WorkPredicate;
import hu.vrea.repository.WorkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class WorkService {

    @Autowired
    WorkRepository workRepository;

    public Optional<Work> findById(Long workId) {
        return workRepository.findById(workId);
    }

    public Work saveWork(Work work) {
        return workRepository.save(work);
    }

    public Page<Work> findAllAdvancedSearch(Long currentUserId, WorkSearchRequest workSearchRequest, QPageRequest qPageRequest) {
        return workRepository.findAll(WorkPredicate.workSearch(currentUserId, workSearchRequest), qPageRequest);
    }

    public int getWorkDoneCountByWorkerId(Long workerId) {
        return workRepository.countByAcceptedAndWorker_IdAndDateTimeInterval_EndDateTimeBefore(true, workerId, LocalDateTime.now());
    }

    public Page<Work> findAllByWorkerId(Long workerId, Pageable pageable) {
        return workRepository.findAllByWorkerIdOrderByCreatedDateDesc(workerId, pageable);
    }

    public Page<Work> findAllByNormalUserId(Long normalUserId, Pageable pageable) {
        return workRepository.findAllByNormalUserIdOrderByCreatedDateDesc(normalUserId, pageable);
    }

    public void setWorkAcceptance(Boolean accepted, Long workerId, Long workId) {
        workRepository.setWorkAcceptance(accepted, workerId, workId);
    }
}
