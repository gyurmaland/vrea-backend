package hu.vrea.service;

import hu.vrea.model.Review;
import hu.vrea.repository.ReviewPredicate;
import hu.vrea.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.stereotype.Service;

@Service
public class ReviewService {

    @Autowired
    ReviewRepository reviewRepository;

    public Page<Review> findReviews(Long reviewedUserId, QPageRequest qPageRequest) {
        return reviewRepository.findAll(ReviewPredicate.reviewSearch(reviewedUserId), qPageRequest);
    }

    public Review saveReviewForWorker(Review review) {
        return reviewRepository.save(review);
    }

    public Double avgWorkerRatingByUserId(Long userId) {
        return reviewRepository.avgWorkerRatingByUserId(userId);
    }
}
