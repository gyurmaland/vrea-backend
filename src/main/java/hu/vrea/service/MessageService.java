package hu.vrea.service;

import hu.vrea.model.Message;
import hu.vrea.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class MessageService {

    @Autowired
    private MessageRepository messageRepository;

    public Message save(Message message) {
        return messageRepository.save(message);
    }

    public Page<Message> findAllByWorkId(Long workId, PageRequest pageRequest) {
        return messageRepository.findAllByWork_IdOrderByCreatedDateDesc(workId, pageRequest);
    }
}
