package hu.vrea.service;

import hu.vrea.model.User;
import hu.vrea.payload.user.UserSearchRequest;
import hu.vrea.repository.UserPredicate;
import hu.vrea.repository.UserRepository;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Objects;
import java.util.Optional;

@Service
public class UserService {

    String datePattern = "yyy-MM-dd";

    @Autowired
    UserRepository userRepository;

    public Optional<User> findUserById(Long userId) {
        return userRepository.findById(userId);
    }

    public Page<User> findWorkers(Long currentUserId, UserSearchRequest userSearchRequest, PageRequest pageRequest) {
        return userRepository.findAll(UserPredicate.workerSearch(currentUserId, userSearchRequest), pageRequest);
    }

    public Page<User> findWorkersOrderedByDistance(Long currentUserId, UserSearchRequest userSearchRequest, PageRequest pageRequest) {
        //TODO: refakt kell majd az tuti
        GeometryFactory gf = new GeometryFactory();
        Point location = gf.createPoint(new Coordinate(userSearchRequest.getLocation().getLng(), userSearchRequest.getLocation().getLat()));
        location.setSRID(4326);

        LocalDateTime searchStartOfDay = null;
        LocalDateTime searchEndOfDay = null;

        //TODO: ezt se itt a legszebb...
        if (!StringUtils.isEmpty(userSearchRequest.getWorkDateTime())) {
            searchStartOfDay = LocalDateTime.parse(userSearchRequest.getWorkDateTime()).with(LocalTime.MIN);
            searchEndOfDay = LocalDateTime.parse(userSearchRequest.getWorkDateTime()).with(LocalTime.MAX);
        }

        String skillName = "";

        // TODO: refakt kell majd, az egészet legjobb lenne query dsl-ben megoldani, de a geogafikus cuccal futni kell még köröket, mert ott nem igazán működik a megfeleő type-al
        if (Objects.nonNull(userSearchRequest.getSkill())) {
            skillName = userSearchRequest.getSkill().name();
            return userRepository.findWorkersOrderedByDistance(location, currentUserId, skillName, searchStartOfDay, searchEndOfDay, pageRequest);
        } else {
            return userRepository.findWorkersOrderedByDistanceWithoutSkillName(location, currentUserId, searchStartOfDay, searchEndOfDay, pageRequest);
        }
    }

    public User saveUser(User user) {
        return userRepository.save(user);
    }
}
