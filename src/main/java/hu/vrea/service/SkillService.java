package hu.vrea.service;

import hu.vrea.model.Skill;
import hu.vrea.model.enums.SkillEnum;
import hu.vrea.repository.SkillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SkillService {

    @Autowired
    SkillRepository skillRepository;

    public Skill findBySkillEnum(SkillEnum skillEnum) {
        return skillRepository.findBySkillEnum(skillEnum);
    }
}
