package hu.vrea.service;

import hu.vrea.model.DateTimeInterval;
import hu.vrea.repository.DateTimeIntervalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class DateTimeIntervalService {

    @Autowired
    private DateTimeIntervalRepository dateTimeIntervalRepository;

    public Optional<DateTimeInterval> findById(Long id) {
        return dateTimeIntervalRepository.findById(id);
    }

    public void save(DateTimeInterval dateTimeInterval) {
        dateTimeIntervalRepository.save(dateTimeInterval);
    }

    public List<DateTimeInterval> saveAll(List<DateTimeInterval> dateTimeIntervalList) {
        return dateTimeIntervalRepository.saveAll(dateTimeIntervalList);
    }

    public void editById(Long dateTimeIntervalId, DateTimeInterval dateTimeInterval) {
        dateTimeIntervalRepository.editById(dateTimeIntervalId, dateTimeInterval.getStartDateTime(), dateTimeInterval.getEndDateTime());
    }

    public List<DateTimeInterval> findByWorkerId(Long userId) {
        return dateTimeIntervalRepository.findByWorkerId(userId);
    }

    public List<DateTimeInterval> findByStartDateTimeBetween(Long userId, LocalDateTime startDateTime, LocalDateTime endDateTime) {
        return dateTimeIntervalRepository.findByStartDateTimeBetweenAndWorkerId(startDateTime, endDateTime, userId);
    }

    @Transactional
    public void deleteById(Long id) {
        dateTimeIntervalRepository.deleteById(id);
    }

    @Transactional
    public void deleteByWorkerId(Long workerId) {
        dateTimeIntervalRepository.deleteByWorkerId(workerId);
    }
}
